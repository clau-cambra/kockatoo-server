from mastodon import Mastodon
from mastodon import MastodonAPIError, MastodonUnauthorizedError
from pathlib import Path
import time

usercred_path = os.environ['CREDS_PATH'] + '/pytooter_usercred.secret'

def log_print(x):
    print ("["+time.strftime("%H:%M:%S")+"] "+x)


def toot(text,api_base_url,image= ''):
    log_print("Posting to mastadon") 
    try:
        mastodon = Mastodon(
            access_token = usercred_path,
            api_base_url = api_base_url
            )
    except MastodonIllegalArgumentError:
        log_print('Mismatch in base URLs between credentials and specified')
    try:
        mastodon.account_verify_credentials()
    except MastodonUnauthorizedError:
        log_print('Please check your credentials and try again')

    if image == '':
        log_print(f"Posting status {text} to {mastodon.api_base_url}")
        response = mastodon.status_post(text)
    else:
        try: 
            log_print("Trying to post Media to mastodon")
            log_print(f"Media selected {image}")
            media_dict = mastodon.media_post(image, mime_type='image/png',description='', focus=None)           
            log_print(str(media_dict))
            log_print("Media successfully uploaded to mastodon")
            log_print(f"Media Id is {media_dict['id']}")
            log_print(f"Posting status {text} to {mastodon.api_base_url}")
            response = mastodon.status_post(text,media_ids=media_dict['id'])
        except MastodonAPIError:
            log_print('Mastodon API Error')
            log_print('media_dict:')
            log_print(media_dict)
            return 
    return response['url']

def mastodon_auth(api_base_url):
    mastodon = Mastodon(
    access_token = usercred_path,
    api_base_url = api_base_url
    )  
    try :
        mastodon.account_verify_credentials()
        return 1
    except:
        MastodonUnauthorizedError
        print(mastodon.account_verify_credentials())
        return 0
    
