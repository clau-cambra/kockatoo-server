from mastodon import Mastodon
from getpass import getpass
from pathlib import Path
import os 

#clientcred_path = Path.cwd() / 'platforms' / 'creds' / 'pytooter_clientcred.secret'
#usercred_path = Path.cwd() / 'platforms' / 'creds' / 'pytooter_usercred.secret'
clientcred_path = os.environ['CREDS_PATH'] + '/pytooter_clientcred.secret'
usercred_path = os.environ['CREDS_PATH'] + '/pytooter_usercred.secret'

def mastodon_initialize():
    print('Setting up mastodon')
    api_base_url = input("Enter the base url of the mastodon instance you are authenticating with: ")
    username = input("Enter your username: ")
    password = getpass(prompt='Enter your password: ')
    
    Mastodon.create_app(
          'pytooterapp',
          api_base_url = api_base_url,
          to_file = clientcred_path
    )
    mastodon = Mastodon(
            client_id = clientcred_path,
            api_base_url = api_base_url
        )
    mastodon.log_in(
            username,
            password,
            to_file = usercred_path
        )
mastodon_initialize()
